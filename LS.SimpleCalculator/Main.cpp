#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

/*float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);

float Add(float num1, float num2)
{
	float answer = num1 + num2;
	return answer;
}

float Subtract(float num1, float num2)
{
	float answer = num1 - num2;
	return answer;
}

float Multiply(float num1, float num2)
{
	float answer = num1 * num2;
	return answer;
}

int main()
{
	float num1, num2, answer;
	int operation;

	cout << "Enter two positive numbers: ";
	cin >> num1 >> num2;

	cout << "Enter one of the following characters: +, -, *, or/ ";
	cin >> operation;


	string cont = "y";

	//do {

		switch (operation)
		{
		case '+':
			Add(num1, num2);
			break;

		case '-':
			Subtract(num1, num2);
			break;

		case '*':
			Multiply(num1, num2);
			break;

			//case '/':
			//	bool Divide(float num1, float num2, float& answer);
			//	break;

		default:
			cout << "Error! Incorrect operator.";
			break;
		}

		//cout << answer;

		cout << "\nTo calculate again, enter 'y': ";

		cin >> cont;

	//}while
		(cont == "y");

	_getch();
	return 0;
}*/


float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float &answer);

float Add(float num1, float num2)
{
	float answer = num1 + num2;
	return answer;
}

float Subtract(float num1, float num2)
{
	float answer = num1 - num2;
	return answer;
}

float Multiply(float num1, float num2)
{
	float answer = num1 * num2;
	return answer;
}

bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0) return false;
	answer = num1 / num2;
	return true;
}


int main()
{
	while (true)

	{
		float num1, num2, answer = 0;
		int operation;

		cout << "\nPlease enter a number\n";
		cin >> num1;
		cout << "Please enter another number\n";
		cin >> num2;

		cout << "Select the operation you would like to perform:";

		cout << "\n1. +\n";
		cout << "2. -\n";
		cout << "3. *\n";
		cout << "4. /\n";

		cin >> operation;

		switch (operation)
		{
		case 1:
			cout << "Answer: " << Add(num1, num2);
			break;
		case 2:
			answer = Subtract(num1, num2);
			cout << "Answer: " << answer;
			break;
		case 3:
			answer = Multiply(num1, num2);
			cout << "Answer: " << answer;
			break;
		case 4:
			if (Divide(num1, num2, answer)) 
			{
				cout << "Answer: " << answer;
			}
			else 
			{
				cout << "You can't divide by zero.";
			}
			break;

		default:
			cout << "Please select a valid operator.";
		}


		char ans;
		do
		{
			cout << "\nDo you want to continue (Y/N)?\n";
			cout << "You must type a 'Y' or an 'N' :";
			cin >> ans;
		} while ((ans != 'Y') && (ans != 'N') && (ans != 'y') && (ans != 'n'));
	
	
		if (ans == 'N' || ans == 'n') return 0;
	}

}
